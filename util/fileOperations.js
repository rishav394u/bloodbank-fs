const fs = require("fs");
var userModel = require("../models/user");

const userFile = "user.json";

setInterval(() => {
  userModel.find({}).then((doc) => {
    fs.writeFileSync(userFile, JSON.stringify(doc, null, 2));
  });
}, 1000);
